Rails.application.routes.draw do
  devise_for :users
# resources :sessions
  resources :users
  root "homes#index"

  get "dashboard" => 'dashboards#index', :as => :dashboard_page

end
