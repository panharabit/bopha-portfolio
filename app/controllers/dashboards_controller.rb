class DashboardsController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, only:[:new,:edit,:update,:destroy]

  def index

  end
end
